// ТЕОРІЯ
/*
1.window.addEventListener("keydown", function(el){...})

2. event.code() є методом, який виводить нам фізичний клавішу клавіатури не дивлячись яка у нас вибрана мова, в той час як event.key() виводить нам лише значення того символу який відповідає нашій вибраній мові.

3.keydown - коли натиснули на клавішу
keyup - коли відтиснули клавішу
keypress - показує що символ був введений. Не генерує клавіши управління 
*/

// ПРАКТИКА

const buttons = document.querySelectorAll('.btn');

window.addEventListener("keydown", function(element){
	buttons.forEach((key) => {
		if(element.key.toUpperCase() != key.textContent.toUpperCase()){
			console.log(key.style.backgroundColor = "black");
		}else{
			console.log(key.style.backgroundColor = "blue");
		}
	})
});
